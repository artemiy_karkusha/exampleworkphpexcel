<?php
session_start();
/**
 * Created by PhpStorm.
 * User: Artemiy Karkusha
 * Date: 25.04.2018
 * Time: 18:59
 */

// Подключаем класс для работы с excel
require_once('Classes/PHPExcel.php');
// Подключаем класс для вывода данных в формате excel
require_once('Classes/PHPExcel/Writer/Excel5.php');
$headerResponse = $_SESSION['headerResponse'];
$border = [
    'borders' => [
        'bottom' => [
            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
            'color' => [
                'rgb' => '808080'
            ]
        ],
        'top' => [
            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
            'color' => [
                'rgb' => '808080'
          ]
        ],
        'left' => [
            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
            'color' => [
            'rgb' => '808080'
            ]
        ],
        'right' => [
            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
            'color' => [
                'rgb' => '808080'
            ]
        ]
    ]
];
//Массив с настройками для выравнивания текста в ячейках
$alignmentCenter = [
    'alignment' => [
        'horizontal' 	=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        'vertical'   	=> PHPExcel_Style_Alignment::VERTICAL_CENTER
    ]
];
$colorRed = [
    'fill' => [
        'type' => 'solid',
        'color' => [
            'rgb' =>'FF3940'
        ]
    ]
];
$colorGreen = [
    'fill' => [
        'type' => 'solid',
        'color' => [
            'rgb' => '00CD0E'
        ]
    ]
];
$colorGrey = [
    'fill' => [
        'type' => 'solid',
        'color' => [
            'rgb' => '8E91EF'
        ]
    ]
];
$fontTop = [
    'font' => [
        'name' => 'Arial',
        'size' => 10,
        'bold' => true
    ]
];
// Создаем объект класса PHPExcel
    $xls = new PHPExcel();
// Устанавливаем индекс активного листа
    $xls->setActiveSheetIndex(0);
// Получаем активный лист
    $sheet = $xls->getActiveSheet();
// Подписываем лист
    $sheet->setTitle('Report');

// Устанавливаем название и оформление столбцов
    $sheet->setCellValue("A1", '№');
    $sheet->getColumnDimension('A')->setWidth(9);
// Выравнивание текста
    $sheet->getStyle('A1')->applyFromArray($alignmentCenter);
    $sheet->setCellValue("B1", 'Название проверки');
    $sheet->getColumnDimension('B')->setWidth(53);
    $sheet->setCellValue("C1", 'Статус');
// Выравнивание текста
    $sheet->getStyle('C1')->applyFromArray($alignmentCenter);
    $sheet->setCellValue("D1", '');
    $sheet->getColumnDimension('D')->setWidth(15);
    $sheet->setCellValue("E1", 'Текущее состояние');
    $sheet->getColumnDimension('E')->setWidth(65);
//Заливаем серым цветом верхние ячейки используя ранее созданый масив $colorGrey и создаем рамки используя массив $border
for ($i = 0; $i < 5; $i++) {
    $sheet->getStyleByColumnAndRow($i, 1)->applyFromArray($colorGrey);
    $sheet->getStyleByColumnAndRow($i,1)->applyFromArray($border);
}
// Объединяем ячейки
    $sheet->mergeCells('A2:E2');
    $sheet->getStyle('A2:E2')->getFill()
        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
        ->getStartColor()->setRGB('E4E4E4');
    $sheet->getStyle('A2:E2')->applyFromArray($border);
    if ($headerResponse['existRobots'] == true){
        $sheet->mergeCells('A3:A4');
        $sheet->setCellValue("A3", '1');
        $sheet->getStyle('A3')->applyFromArray($alignmentCenter);
        $sheet->mergeCells('B3:B4');
        $sheet->setCellValue("B3", 'Проверка наличия файла robots.txt');
        $sheet->getStyle('B3')->applyFromArray($alignmentCenter);
        $sheet->mergeCells('C3:C4');
        $sheet->setCellValue("C3", 'Ок');
        $sheet->getStyle('C3')->applyFromArray($alignmentCenter);
        $sheet->getStyle('C3')->applyFromArray($colorGreen);
        $sheet->setCellValue('D3','Состояние');
        $sheet->setCellValue('D4','Рекомендации');
        $sheet->setCellValue('E3','Файл robots.txt присутствует');
        $sheet->setCellValue('E4','Доработки не требуются');
        //Рамки
        for ($i = 0; $i < 5; $i++) {
            for ($j = 3; $j < 5; $j++) {
                $sheet->getStyleByColumnAndRow($i, $j)->applyFromArray($border);
            }
        }
    }else{
        $sheet->mergeCells('A3:A4');
        $sheet->setCellValue("A3", '1');
        $sheet->getStyle('A3')->applyFromArray($alignmentCenter);
        $sheet->mergeCells('B3:B4');
        $sheet->setCellValue("B3", 'Проверка наличия файла robots.txt');
        $sheet->getStyle('B3')->applyFromArray($alignmentCenter);
        $sheet->mergeCells('C3:C4');
        $sheet->setCellValue("C3", 'Ок');
        $sheet->getStyle('C3')->applyFromArray($alignmentCenter);
        $sheet->getStyle('C3')->applyFromArray($colorGreen);
        $sheet->setCellValue('D3','Состояние');
        $sheet->setCellValue('D4','Рекомендации');
        $sheet->setCellValue('E3','Файл robots.txt присутствует');
        $sheet->setCellValue('E4','Доработки не требуются');
        //Рамки
        for ($i = 0; $i < 5; $i++) {
            for ($j = 3; $j < 5; $j++) {
                $sheet->getStyleByColumnAndRow($i, $j)->applyFromArray($border);
            }
        }
    }
    $objWriter = new PHPExcel_Writer_Excel5($xls);
    header('Content-type: application/vnd.ms-excel;charset=utf-8');
    header('Content-Disposition: attachment; filename="report.xls"');
    $objWriter->save('php://output');

