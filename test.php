<?php
/**
 * Created by PhpStorm.
 * User: Artemiy Karkusha
 * Date: 25.04.2018
 * Time: 18:59
 */
ob_start();
session_start();
include 'header.php';
function parse_url_if_valid($url)
{
    // Массив с компонентами URL, сгенерированный функцией parse_url()
    $arUrl = parse_url($url);
    // Возвращаемое значение. По умолчанию будет считать наш URL некорректным.
    $ret = null;
    // Если не был указан протокол, или
    // указанный протокол некорректен для url
    if (!array_key_exists("scheme", $arUrl)
        || !in_array($arUrl["scheme"], array("http", "https")))
        // Задаем протокол по умолчанию - http
        $arUrl["scheme"] = "https";

    // Если функция parse_url смогла определить host
    if (array_key_exists("host", $arUrl) &&
        !empty($arUrl["host"]))
        // Собираем конечное значение url
        $ret = sprintf("%s://%s%s", $arUrl["scheme"],
            $arUrl["host"]);

    // Если значение хоста не определено
    // (обычно так бывает, если не указан протокол),
    // Проверяем $arUrl["path"] на соответствие шаблона URL.
    else if (preg_match("/^\w+\.[\w\.]+(\/.*)?$/", $arUrl["path"]))
        // Собираем URL
        $ret = sprintf("%s://%s", $arUrl["scheme"], $arUrl["path"]);

    return $ret;
}

function ExistFile($url)
{
    // файл, который мы проверяем
    $url .= '/robots.txt';
    $headers = @get_headers($url);
    /*echo "<pre>";
    print_r($headers);
    echo "<pre>";*/
    $str = preg_replace('/\D/', '', $headers[0]);
    $code = substr($str, 2);
    if ($code == 200 or $code == 301) {
        $existHost  = strripos(file_get_contents($url), 'Host');
        if ( $existHost !== false){
            return $headerResponse = [
                'Code' => $code,
                'Length' => $headers['Content-Length'],
                'existHost' => 'true',
                'existRobots' => 'true',

            ];
        } else {
            return $headerResponse = [
                'Code' => $code,
                'Length' => $headers['Content-Length'],
                'existHost' => 'false',
                'existRobots' => 'false',
            ];

        }
    } else {
        echo $url;
        return $headerResponse = [
            'existRobots' => 'false'
        ];
    }

}


?>

<?php

if (!empty($_POST['site-url'])) {
    $url = parse_url_if_valid($_POST['site-url']);
    if (!$url) {
        // Введен некорректный URL
        header('Location:index.php?test=false');
        exit();
    } else {
        // Работаем с полученным значением, как нам удобно.
        //echo "Ваш url: " . $url . "<br>";
        $headerResponse = ExistFile($url);
        $_SESSION['headerResponse'] = $headerResponse;
        ?>
        <div id="welcome">
            <h1>Результат перевірки сайту,<?php echo " " . $url?></h1><br>
            <p><a href="/excelWrite.php">Скачати файли</a></p>
        </div>
        <?
    }
}
?>

<?
    include 'footer.php';
    ob_flush();
?>